import React, {useState, useEffect} from "react";
// import { Component } from "react";

// import logo from './logo.svg';
import "./App.css";
import CardList from "./components/cardList/CardList.component";
import SearchBox from "./components/searchBox/SearchBox.components";



const App = () => {
    
    const [searchField, setSearchField] = useState("");
    const [title, setTitle] = useState("Monsters Rolodex");
    const [monsters, setMonsters] = useState([]);
    const [filteredMonsters, setFilteredMonsters] = useState(monsters);
    

    // console.log("Component Rendering");
    // console.log(searchField);
    
    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/users")
        .then((resp) => resp.json())
        .then((users) => setMonsters(users))

    }, []);

    useEffect(() => {
        const newFilteredMonsters = monsters.filter((monster) => {
            return monster.name.toLocaleLowerCase().includes(searchField);
        });
        setFilteredMonsters(newFilteredMonsters);
        // console.log("newFilteredMonsters");
    }, [monsters, searchField]);
    
    

    const onSearchChange = (event) => {
        // console.log(event.target.value);
        const searchStr = event.target.value.toLocaleLowerCase();
        setSearchField(searchStr);
        
    }
    const onTitleChange = (event) => {
        // console.log(event.target.value);
        const searchStr = event.target.value.toLocaleLowerCase();
        setTitle(searchStr);
        
    }


    return (
        <div className="App">
            <h1 className="appTitle" >{title}</h1>
            <SearchBox onSearchChange={onSearchChange} className="searchBox" placeholder="Search Here" /><br />
            <SearchBox onSearchChange={onTitleChange} className="searchBox" placeholder="Title Change Here" />
            <CardList monsters={filteredMonsters} />
        </div>
    );
}



// class App extends Component {
//     constructor() {
//         super();

//         this.state = {
//             monsters: [],
//             searchStr: "",
//         };
//         // console.log("Constructor");
//     }

//     componentDidMount = () => {
//         // console.log("Component Did Mount");
//         fetch("https://jsonplaceholder.typicode.com/users")
//             .then((resp) => resp.json())
//             .then((users) =>
//                 this.setState(
//                     () => {
//                         return {
//                             monsters: users,
//                         };
//                     },
//                     () => {
//                         // console.log(this.state);
//                     }
//                 )
//             );
//     }

//     onSearchChange = (event) => {
//         // console.log(event.target.value);
//         const searchStr =
//             event.target.value.toLocaleLowerCase();

//         this.setState(() => {
//             return { searchStr };
//         });
//     }

//     render() {
//         const {monsters, searchStr} = this.state;
//         const {onSearchChange} = this;
//         // console.log("Render");
//         const filteredMonsters = monsters.filter((monster) => {
//             return monster.name
//                 .toLocaleLowerCase()
//                 .includes(searchStr);
//         });
//         return (
//             <div className="App">
//                 <h1 className="appTitle" >Monsters Rolodex</h1>
//                 <SearchBox onSearchChange={onSearchChange} className="searchBox" placeholder="Search Here" />
//                 <CardList monsters={filteredMonsters} />
                
//             </div>
//         );
//     }
// }

export default App;
