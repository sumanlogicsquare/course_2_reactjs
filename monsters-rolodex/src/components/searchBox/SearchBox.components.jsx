import React from "react";
// import React, { Component } from "react";
import "./SearchBox.styles.css";

const SearchBox = (props) => {
    const { className, placeholder, onSearchChange } = props;
    return (
        <input
            className={`searchBox ${className}`}
            type="search"
            placeholder={placeholder}
            onChange={onSearchChange}
        />
    );
};

// class SearchBox extends Component {
//     render() {
//         const { className, placeholder, onSearchChange } = this.props;
//         return (
//             <input
//                 className={`searchBox ${className}`}
//                 type="search"
//                 placeholder={placeholder}
//                 onChange={onSearchChange}
//             />
//         );
//     }
// }

export default SearchBox;
