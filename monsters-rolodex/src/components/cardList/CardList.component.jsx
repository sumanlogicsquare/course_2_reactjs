import React from "react";
// import React, { Component } from "react";
import Card from "../card/Card.component";

import "./CardList.styles.css";

const CardList = (props) => {
    const { monsters } = props;
    // console.log(this.props);
    return (
        <div className="cardList">
            {monsters.map((monster) => {
                return <Card key={monster.id} monster={monster} />;
            })}
        </div>
    );
};

// class CardList extends Component {
//     render() {
//         const { monsters } = this.props;
//         // console.log(this.props);
//         return (
//             <div className="cardList">
//                 {monsters.map((monster) => {
//                     return <Card monster={monster} />;
//                 })}
//             </div>
//         );
//     }
// }

export default CardList;
