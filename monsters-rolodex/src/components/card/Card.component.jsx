import React from "react";

import "./Card.styles.css";

const Card = ({ monster: { id, name, email } }) => {
    return (
        <div className="cardContainer">
            <img
                src={`https://robohash.org/${id}?set=set2&size=180x180`}
                alt={`Monster: ${name}`}
            />
            <h1>{id + ": " + name}</h1>
            <h1>{email}</h1>
        </div>
    );
};

// class Card extends Component {
//     render() {
//         const { name, id, email } = this.props.monster;
//         return (
//             <div key={id} className="cardContainer">
//                 <img
//                     src={`https://robohash.org/${id}?set=set2&size=180x180`}
//                     alt={`Monster: ${name}`}
//                 />
//                 <h1>{id + ": " + name}</h1>
//                 <h1>{email}</h1>
//             </div>
//         );
//     }
// }

export default Card;
