import React, { useContext } from 'react';
import { CartContext } from '../../contexts/Cart.context';
import "./checkoutItem.styles.scss";

const CheckoutItem = ({cartItem}) => {

    const {name, imageUrl, price, quantity} = cartItem;
    const {clearItemFromCart, addItemToCart, removeItemFromCart} = useContext(CartContext);
    const clearItemHandler = () => clearItemFromCart(cartItem);
    const addItemHandler = () => addItemToCart(cartItem);
    const removeItemHandler = () => removeItemFromCart(cartItem);

    return (
    <div className="checkoutItemContainer">
        <div className="imageContainer">
            <img src={imageUrl} alt={`${name}`} />
        </div>
        <span className="name"> {name} </span>
        <span className="quantity"> 
            <div className="arrow" onClick={removeItemHandler}>
            &#8722;
            </div>
            <span className='value'>{quantity}</span> 
            <div className="arrow" onClick={addItemHandler}>
            &#43;
            </div>
        </span>
        <span className="price"> {price} </span>
        <div className='removeButton' onClick={clearItemHandler}>&#10008;</div>
    </div>
  )
}

export default CheckoutItem;


// <div key={id}>
//                     <h2>{name}</h2>
//                     <span>{quantity}</span>     
//                     <spam onClick={() => addItemToCart(cartItem)}>Decrement</spam>
//                     <span onClick={() => removeItemFromCart(cartItem)}>Increment</span>
//                 </div>