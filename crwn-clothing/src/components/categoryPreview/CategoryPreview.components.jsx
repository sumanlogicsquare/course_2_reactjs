import React from "react";
import { Link } from "react-router-dom";
import ProductCard from "../productCard/ProductCard.component";

import "./categoryPreview.styles.scss";

const CategoryPreview = ({ title, products }) => {
    return (
        <div className="categoryPreviewContainer">
            <h2>
                <Link className="title" to={title}>
                    {title.toUpperCase()} &#8680;
                </Link>
            </h2>
            <div className="preview">
                {products
                    .filter((_, index) => index < 4)
                    .map((product) => {
                        return (
                            <ProductCard key={product.id} product={product} />
                        );
                    })}
            </div>
        </div>
    );
};

export default CategoryPreview;
