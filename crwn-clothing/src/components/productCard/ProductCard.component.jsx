import React, { useContext } from "react";
import { CartContext } from "../../contexts/Cart.context";
import Button, { BUTTON_TYPE_CLASSES } from "../button/Button.component";
import "./productCard.styles.scss";

const ProductCard = ({ product }) => {
    const { name, price, imageUrl } = product;
    const { addItemToCart } = useContext(CartContext);
    const addProductToCart = () => addItemToCart(product);

    return (
        <div className="productCardContainer">
            <img src={imageUrl} alt={`${name}`} />
            <div className="footer">
                <span className="name">{name}</span>
                <span className="price">{price}</span>
            </div>
            <Button
                buttonType={BUTTON_TYPE_CLASSES.inverted}
                onClick={addProductToCart}
            >
                Add to Cart
            </Button>
        </div>
    );
};

export default ProductCard;
