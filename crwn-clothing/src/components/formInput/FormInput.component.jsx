import React from "react";
import { FormInputLebel, Group, Input } from "./formInput.styles";

const FormInput = (props) => {
    const { label, ...otherProps } = props;
    return (
        <Group>
            <Input {...otherProps} />
            {label && (
                <FormInputLebel shrink={otherProps.value.length}>
                    {label}
                </FormInputLebel>
            )}
        </Group>
    );
};

export default FormInput;
