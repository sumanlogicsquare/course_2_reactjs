import React, { useContext } from 'react'
import CheckoutItem from '../../components/checkoutItem/CheckoutItem.component';
import { CartContext } from '../../contexts/Cart.context';

import "./checkout.styles.scss";

const Checkout = () => {
    const {cartItems, cartTotal} = useContext(CartContext);
  return (
    <div className="checkoutContainer">
        <div className="checkoutHeader" >
            <div className="headerBlock">
                <span>Product</span>
            </div>
            <div className="headerBlock">
                <span>Description</span>
            </div>
            <div className="headerBlock">
                <span>Quantity</span>
            </div>
            <div className="headerBlock">
                <span>Price</span>
            </div>
            <div className="headerBlock">
                <span>Remove</span>
            </div>
        </div>
        {cartItems.map((cartItem) => {
            return(
                <CheckoutItem key={cartItem.id} cartItem={cartItem} />
            )
        })}
        <span className="total">Total : ${cartTotal}</span>
    </div>
  )
}

export default Checkout;