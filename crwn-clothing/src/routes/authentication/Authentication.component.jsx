import React from "react";
import Button from "../../components/button/Button.component";
import SignInForm from "../../components/signInForm/SignInForm.component";
import SignUpForm from "../../components/signUpForm/SignUpForm.component";

import "./authentication.styles.scss";

// import React, { useEffect } from 'react'
// import { getRedirectResult } from 'firebase/auth';
// import {
//     sighInWithGooglePopup,
//     createUserDocumentFromAuth,
// } from "../../utils/firebase/firebase.utils";

// import { auth, sighInWithGooglePopup, signInWithGoogleRedirect, createUserDocumentFromAuth } from "../../utils/firebase/firebase.utils";

const Authentication = () => {
    // useEffect(async ()=> {
    //   const resp =  await getRedirectResult(auth);
    //   if(resp){
    //     const userDocRef = await createUserDocumentFromAuth(resp.user);
    //   }
    // }, []);

    return (
        <div className="authenticationContainer">
            <SignInForm />
            <SignUpForm />

            {/* <button onClick={signInWithGoogleRedirect}>
           Sign with Google Redirect
        </button> */}
        </div>
    );
};

export default Authentication;
