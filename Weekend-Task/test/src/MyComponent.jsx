// Weekend Task: do it in your fee time just to check your functional components & hoc knowledge
// make the below class  based component into a fully functional component or Hook
import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import userEvent from "@testing-library/user-event";

const WindowWidth = ({ children }) => {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth);

    // const isMount = useRef();

    useEffect(() => {
        // if (isMount.current) return;
        window.addEventListener("resize", onResize);
        console.log("addEvent");
        // isMount.current = true;

        return () => {
            window.removeEventListener("resize", onResize);
            console.log("removeEvent");
        };
    }, []);

    const onResize = () => {
        setWindowWidth(window.innerWidth);
    };

    return children(windowWidth);
};

WindowWidth.propTypes = {
    children: PropTypes.func.isRequired,
};

export default WindowWidth;

// SANDBOX
// https://codesandbox.io/s/optimistic-villani-8c2s95?file=/src/App.js

// //////////////////////////////////
// //////////////////////////////////
// //////////////////////////////////
// //////////////////////////////////

// class WindowWidth extends React.Component {
//     propTypes = {
//         children: PropTypes.func.isRequired,
//     };

//     state = {
//         windowWidth: window.innerWidth,
//     };

//     onResize = () => {
//         this.setState({
//             windowWidth: window.innerWidth,
//         });
//     };

//     componentDidMount() {
//         window.addEventListener("resize", this.onResize);
//     }

//     componentWillUnmount() {
//         window.removeEventListener("resize", this.onResize);
//     }

//     render() {
//         return this.props.children(this.state.windowWidth);
//     }
// }

// // To be used like this:

// const MyComponent = () => {
//     return (
//         <WindowWidth>
//             {(width) => <div>Window width is: {width}</div>}
//         </WindowWidth>
//     );
// };
