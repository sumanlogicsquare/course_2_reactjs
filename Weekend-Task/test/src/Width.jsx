import React from "react";
import WindowWidth from "./MyComponent";

const Width = () => {
    const getWidth = (width) => <div>Window width is: {width}</div>;

    return <WindowWidth>{getWidth}</WindowWidth>;
};

export default Width;
