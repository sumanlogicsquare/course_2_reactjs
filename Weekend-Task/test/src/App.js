// import logo from './logo.svg';
import { useState } from 'react';
import './App.css';
// To be used like this:

import Width from './Width';

function App() {
  const [displayWindowWidth, setDisplayWindowWidth] = useState(true)
  
  const toggleWidth = () => setDisplayWindowWidth(currentState => !currentState)
  
  return (
    <div className="App">
      {displayWindowWidth && 
        <Width />
      }
      {/* <button onClick={() => setDisplayWindowWidth(!displayWindowWidth)} > Toggle the Width </button> */}
      <button onClick={toggleWidth} > Toggle the Width </button>
    </div>
  );
}

export default App;

